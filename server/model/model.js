const mongoose = require('mongoose');

var schema = new mongoose.Schema({
    name:{
        type:String,
        required:true,
        
    },
    age:{
        type:Number,
        required:true,
    },
    gender:{
        type:String,
        required:true,
    },
   
    contact:{
        type:Number,
        required:true,
        
    },
    email:{
        type:String,
        required:true,
        
        
    },


})

const Userdb = mongoose.model('userdb',schema);

module.exports = Userdb;