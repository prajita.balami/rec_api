var Userdb = require('../model/model');


Userdb.ensureIndexes({gender:"text"});



//create and save new employee
exports.create = (req, res) => {
    //validate request
    if (!req.body) {
        res.status(400).send({ message: "Any field cannot be empty" });
        return;
    }

    //new employee
    const user = new Userdb({
        name: req.body.name,
        age:req.body.age,
        gender:req.body.gender,
        contact: req.body.contact,
        email: req.body.email
    })

    //save user in database
    user
        .save(user)
        .then(data => {
            res.send(data)
           res.redirect('/add_user')  //saves all data from form to mongo db
        })
        .catch(error => {
            res.status(500).send({
                message: error.message || "Something went wrong"
            });

        });

}

//retrive and return all employee/ retrive and return a single user
exports.find = (req, res) => {
    if(req.query.id){
        console.log("REached here");
        const id = req.query.id;

        Userdb.findById(id)
            .then(data=>{
                if(!data){
                    res.status(404).send({message:"User not found"})
                }
                else{
                    res.send(data)
                }
            })
            .catch(err=>{
                res.status(500).send({message:"Error retriving id "+id});

            })

    }
    else {
        Userdb.find()
            .then(user => {
                res.send(user)
            })
            .catch(error => {
                res.status(500).send({ message: error.message || "Something went wrong" });
            });

    }


}

//update a new employee by id
exports.update = (req, res) => {
    if (!req.body) {
        return res
            .status(400)
            .send({ message: "Data to update cannot be empty" })

    }

    const id = req.params.id;
    Userdb.findByIdAndUpdate(id, req.body, { useFindAndModify: false })
        .then(data => {
            if (!data) {
                res.satus(404).send({ message: `Cannot update user with ${id}` })

            }
            else {
                res.send(data)
            }
        })
        .catch(err => {
            res.status(500).send({ message: "Something went wrong" })
        })

}

//Delete an employee with specified user id in request
exports.delete = (req, res) => {
    const id = req.params.id;

    Userdb.findByIdAndDelete(id)
        .then(data => {
            if (!data) {
                res.status(404).send({ message: `Cannot delete with ${id}` })
            }
            else {
                res.send({
                    message: "User was deleted succesfully"
                })
            }
        })
        .catch(err => {
            res.status(500)({
                message: "Could not delete id" + id
            });
        });

}

exports.search=(req,res)=>{
    const name = req.params.name;
    var regex = new RegExp(name,'i');
    Userdb.find({name:regex})
    .then((result)=>{
        res.status(200).json(result)
    })
   

}
exports.gender=(req,res)=>{
    const name = req.params.name;
    var regex = new RegExp(name,'i');
    Userdb.find({$text:{$search:"male"}})
    .then((result)=>{
        res.status(200).json(result)
    })
   

}

