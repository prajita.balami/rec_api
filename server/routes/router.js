const express = require('express');
const route = express.Router();

const controller = require('../controller/controller')

 


//API
route.use(express.json());
route.post('/api/users',controller.create);
route.get('/api/users',controller.find);
route.put('/api/users/:id',controller.update);
route.delete('/api/users/:id',controller.delete);
route.get('/api/users/search/:name',controller.search);


module.exports = route;