const express = require('express');
const dotenv = require('dotenv');
const morgan = require('morgan');
const bodyparser = require('body-parser');
const path = require('path');

const connectDB = require('./server/database/connection')

const app = express();

var cors = require('cors');
 
app.use(cors());
dotenv.config({path:'config.env'})

const PORT = process.env.PORT||8080

// app.get('/', function (req, res,) {
//     res.json({msg: 'This is CORS-enabled for all origins!'})
//   })

//log request
app.use(morgan('tiny'));

//mongodb connection
connectDB();

//parse request to bodyparser
app.use(bodyparser.urlencoded({extended:true}));

//set view engine
 app.set("view engine","ejs");
// app.set("views",path.resolve(__dirname, "views/html"))

// app.set('views', __dirname + '/views');
// app.engine('html', require('ejs').renderFile);
// app.set('view engine', 'html');

//load asset
app.use('/css', express.static(path.resolve(__dirname, "assets/css")))
app.use('/js', express.static(path.resolve(__dirname, "assets/js")))

//load routers
app.use('/',require('./server/routes/router'))



app.listen(8080, ()=>{console.log(`server running on http://localhost:${PORT}`)});
    //, ()=>{console.log(`Server is running on http://localhost:3000`)});

//app.listen(8080);